opentoken (3.0b-11) unstable; urgency=low

  * Include missing language lexer code.
  * Update to Standards-Version 3.8.2 (no changes).

 -- Reto Buerki <reet@codelabs.ch>  Fri, 19 Jun 2009 00:54:05 +0200

opentoken (3.0b-10) unstable; urgency=low

  * Fix wrong-section-according-to-package-name warning.
    - opentoken-dbg: devel -> debug.
  * Tighten dependency to gnat-4.3 (Closes: #533156).
  * Remove unused opentoken-dbg lintian override.
  * Update to Standards-Version 3.8.1 (no changes).
  * Update Debian specific OpenToken project file.

 -- Reto Buerki <reet@codelabs.ch>  Mon, 15 Jun 2009 22:02:36 +0200

opentoken (3.0b-9) unstable; urgency=low

  * Update maintainer e-mail address.
  * Remove "section" field from binary package libopentoken4.

 -- Reto Buerki <reet@codelabs.ch>  Sat, 07 Feb 2009 12:26:58 +0100

opentoken (3.0b-8) unstable; urgency=low

  * Updated to Standards-Version: 3.8.0
  * Added mips and mipsel to 'Architecture' (Closes: #495239).

 -- Reto Buerki <buerki@swiss-it.ch>  Sat, 16 Aug 2008 18:11:21 +0200

opentoken (3.0b-7) unstable; urgency=low

  * Fix compilation issues in ASU_Example_5_10 parser examples
    (Closes: #249915, #249916).

 -- Reto Buerki <buerki@swiss-it.ch>  Tue, 13 May 2008 14:04:32 +0000

opentoken (3.0b-6) unstable; urgency=low

  * Taking over package from Ludovic Brenta (Closes: #477322).
  * Renamed source package to its upstream name.
  * Added debug package opentoken-dbg.
  * Placed DH_COMPAT in debian/compat (removed export from debian/rules).
  * Upgrading package to debhelper 7.
  * Using dpatch for patch management.
  * Changed SONAME to libopentoken.so.4 because of ABI change
    (transition to gnat-4.3) - no other packages affected.
  * Cleanup of debian/rules.

 -- Reto Buerki <buerki@swiss-it.ch>  Sat, 3 May 2008 10:32:08 +0200

libopentoken (3.0b-5) unstable; urgency=medium

  * debian/control (libopentoken3.0b): conflict with, and replace
    libopentoken3.  Closes: #388147.  Thanks, Bill, for reporting this.

 -- Ludovic Brenta <lbrenta@debian.org>  Tue, 19 Sep 2006 23:40:42 +0200

libopentoken (3.0b-4) unstable; urgency=low

  [Felipe Augusto van de Wiel (faw) <felipe@cathedrallabs.org>]
  * Transition to GCC 4.1.  Closes: #378715.
    - Bump SONAME to 3.0b:
      + changes in debian/rules to allow the use of another SONAME and to
        properly handle the libs
    - Dependency changes in debian/control
      + libopentoken build-depends on gnat (>= 4.1)
      + libopentoken-dev depends on gnat-4.1
      + depends on libopentoken3.0b (to match the new SONAME)
        - rename package libopentoken3 to libopentoken3.0b
  * Changed maintainer e-mail address (requested by him).
  * Removed Uploaders fields (requested by the maintainer).
  * Bump Standards-Version to 3.7.2 (no changes required).
  * Adding architectures supported by gnat-4.1:
    - New arches: amd64 hppa ia64

  [Ludovic Brenta]
  * Many thanks to Felipe for all the hard work.
  * Add alpha and s390, libgnat has just been enabled on these archs.
  * Update the FSF's address in the copyright file.

 -- Ludovic Brenta <lbrenta@debian.org>  Sun,  6 Aug 2006 22:44:15 +0200

libopentoken (3.0b-3) unstable; urgency=low

  * debian/rules (regexp): accept any character in the Debian upload number.
    Closes: #359898.

 -- Ludovic Brenta <ludovic@ludovic-brenta.org>  Sat,  1 Apr 2006 13:46:26 +0200

libopentoken (3.0b-2) unstable; urgency=low

  * debian/control: add support for GNU/kFreeBSD, per request from
    Aurélien Jarno.  Build-depend on gnat (>= 3.15p-19).  Closes: #345191.
  * debian/control: update standards-version to 3.6.2 with no changes.
  * debian/control: update maintainer's email address.
  * debian/libopentoken-dev.postinst: remove, no longer needed.

 -- Ludovic Brenta <ludovic@ludovic-brenta.org>  Thu,  9 Feb 2006 18:20:39 +0100

libopentoken (3.0b-1) unstable; urgency=low

  * Initial Release.  Closes: #342473.

 -- Ludovic Brenta <ludovic.brenta@insalien.org>  Fri, 13 Feb 2004 02:39:47 +0100
